// Made by:
// Oussama Danba - s4435591
// Erin van der Veen - s4431200
// MIT LICENSE

#include <RCSwitch.h>
#include <Servo.h>

// Associated codes of our sockets. We used sockets from EURODOMEST (972080).
// Two of these sockets are first set to 1 and 2 respectively (by holding the ON button on the socket)
// so that we can use the codes of socket 1 and 2. There are two codes that all sockets listen to.
#define OnCode1 12204911
#define OffCode1 12204910
#define OnCode2 12204909
#define OffCode2 12204908
#define OnCodeBoth 12204898
#define OffCodeBoth 12204897

#define CodeLength 24
#define SendPin A5

#define ProtocolNr 1

// RCSwitch allows defining that there is no receiving.
#define RCSwitchDisableReceiving

// Pins for the Ultrasonic Sensor
#define trigPin A2
#define echoPin A1

enum Key {
  RIGHT,
  UP,
  DOWN,
  LEFT,
  NO_KEY
};

RCSwitch mySwitch = RCSwitch();
Servo myServo = Servo();
unsigned long previousTime = 0;
int timeRemaining = 0;
double servoAngle = 0.5;
// SocketX represents the current state of the lights
// SocketX_r represents the value that was given to the lights by the buttons
bool Socket1, Socket2, Socket1_r, Socket2_r;

// Function that returns the key pressed by reading the value of A0 and converting the value to a respective key.
Key getkey() {
  int val = analogRead(A0);
  const int KeyCount = 4;
  const int KeyLimits[KeyCount] = {50, 190, 380, 555};
  const Key KeyEnum[KeyCount] = {RIGHT, UP, DOWN, LEFT};

  for (int i = 0; i < KeyCount; i++) {
    if (val < KeyLimits[i])
      return KeyEnum[i];
  }
  return NO_KEY;
}

// This function send the associated codes using the RCSwitch. If both sockets need to be turned
// on or off we make use of the codes OnCodeBoth and OffCodeBoth so that we only have to send once.
// When the New Devices Lab is busy it is hard to reach the sockets (they will frequently not receive transmissions)
// so reducing the amount of transmissions helps.
void modifySockets() {
  if(Socket1 == Socket2) {
    mySwitch.send(Socket1 ? OnCodeBoth : OffCodeBoth, CodeLength);
  } else {
    mySwitch.send(Socket1 ? OnCode1 : OffCode1, CodeLength);
    delay(150);
    mySwitch.send(Socket2 ? OnCode2 : OffCode2, CodeLength);
  }
}

// Initialize attached devices
void setup() {
  pinMode(SendPin, OUTPUT);
  mySwitch.enableTransmit(SendPin);
  mySwitch.setProtocol(ProtocolNr);
  // To improve reliability we transmit more often than the default 10
  mySwitch.setRepeatTransmit(15);
  mySwitch.setPulseLength(150);

  myServo.attach(A4);

  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);  
}

void loop() {
  if (timeRemaining <= 0) {
    if (Socket1 != Socket1_r || Socket2 != Socket2_r) {
      // We know the the timer ran out, meaning that we must restore the previously entered light configuration
      Socket1 = Socket1_r;
      Socket2 = Socket2_r;
      modifySockets();
    }
    // Retrieve the key
    Key key = getkey();
    // Modify the sockets based on which key was pressed
    switch (key) {
      case RIGHT:
        Socket1 = Socket1_r = false;
        Socket2 = Socket2_r = true;
        modifySockets();
        break;
      case UP:
        Socket1 = Socket1_r = Socket2 = Socket2_r = true;
        modifySockets();
        break;
      case DOWN:
        Socket1 = Socket1_r = Socket2 = Socket2_r = false;
        modifySockets();
        break;
      case LEFT:
        Socket1 = Socket1_r = true;
        Socket2 = Socket2_r = false;
        modifySockets();
        break;
      default:
        break;
    }
  } else {
    // If the time hasn't run out yet, substract the passed time from the remaining time
    timeRemaining -= millis() - previousTime;
  }

  // Sonar block
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  int distance = pulseIn(echoPin, HIGH) / 2 / 34;
  if (distance > 1 && distance < 5) {
    // If the distance is within bounds
    // Turn on Socket1 or Socket2
    Socket1_r ? (Socket2 = true) : (Socket1 = true);
    // Reset the time remaining to 5 seconds
    timeRemaining = 5000;
    modifySockets();
  }

  // Servo block
  if(millis() - previousTime > 50.0) {
    // Move the servor slightly
    // The servo doesn't move if the arduino is currently sending a command to the sockets
    // A solution based on time would "jump"
    previousTime = millis();
    servoAngle += 0.01;
    servoAngle = servoAngle > 1.0 ? -1 : servoAngle;
    myServo.write((int) (abs(servoAngle) * 140 + 20));
  }
}
