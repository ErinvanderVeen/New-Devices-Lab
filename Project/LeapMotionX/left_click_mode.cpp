#include "left_click_mode.hpp"

using namespace Leap;

bool LeftClickMode::handTakeControl(const Hand& hand) {
	const FingerList fingers = hand.fingers();

	FingerList::const_iterator fl = fingers.begin();
	// All fingers except the thumb and index finger must not be extended
	for (std::advance(fl, 2); fl != fingers.end(); ++fl) {
		const Finger finger = *fl;
		if (finger.isExtended()) {
			return false;
		}
	}

	// Since this FingerList comes from a Hand we can assume there is only one of each finger
	Finger index = fingers.fingerType(Finger::TYPE_INDEX)[0];
	Finger thumb = fingers.fingerType(Finger::TYPE_THUMB)[0];
	// Index finger must be extended for clicking to work
	return index.isExtended() && thumb.tipPosition().distanceTo((index.bone(Bone::TYPE_PROXIMAL)).center()) < 24.0f;
}

// Take control when the tip of the thumb is near the joint of the index
// finger. Uses PASS control mode so other things can be done while clicking
InputMode::Control LeftClickMode::takeControl(const Frame& frame) {
	bool all_hands_skip = true;

	for (auto hand : frame.hands()) {
		if (handTakeControl(hand)) {
			all_hands_skip = false;
			if (!mouse_down)
				return PASS;
		}
	}

	if (mouse_down && all_hands_skip) {
		return PASS;
	}

	return SKIP;
}

void LeftClickMode::action(const Frame& frame) {
	if (mouse_down) {
		system("xdotool mouseup 1");
		mouse_down = false;
	} else {
		system("xdotool mousedown 1");
		mouse_down = true;
	}
}
