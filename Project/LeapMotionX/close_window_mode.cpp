#include "close_window_mode.hpp"
#include <numeric>

using namespace Leap;

CloseWindowMode::CloseWindowMode(SharedMemory* shared_memory) {
	this->shared_memory = shared_memory;
}

std::pair<double, double> CloseWindowMode::calculateFingerDistance(const Hand& hand) {
	const FingerList fingers = hand.fingers();

	// Calculate the middle point of the fingers
	Vector middle_point;
	for (auto finger : fingers) {
		middle_point += finger.tipPosition();
	}
	middle_point /= fingers.count();

	// Calculate the distance of each finger to the middle point
	std::vector<float> distances;
	for (auto finger : fingers) {
		distances.push_back(middle_point.distanceTo(finger.tipPosition()));
	}

	// Calculate the mean distance from the middle point
	// This helps us know how far open the hand is
	double sum = std::accumulate(distances.begin(), distances.end(), 0.0);
	double mean = sum / distances.size();

	// Calculate the standard deviation of the finger distances. If this
	// is low we know the fingers are spread evenly around the middle point
	double sq_sum = std::inner_product(distances.begin(), distances.end(), distances.begin(), 0.0);
	double std_dev = std::sqrt(sq_sum / (distances.size() - 1) - mean * mean * distances.size() / (distances.size() - 1));

	return std::make_pair(mean, std_dev);
}

bool CloseWindowMode::handOpen(const Hand& hand) {
	std::pair<double, double> distance = calculateFingerDistance(hand);
	return distance.first > 45.0f && distance.second < 23.0f;
}

bool CloseWindowMode::handClosed(const Hand& hand) {
	std::pair<double, double> distance = calculateFingerDistance(hand);
	return distance.first < 30.0f && distance.second < 10.0f;
}

InputMode::Control CloseWindowMode::takeControl(const Frame& frame) {
	// We are already doing the opening gesture which can interfere with the
	// closing gesture so we simply don't consider the closing gesture
	if (shared_memory->opening_gesture)
		return SKIP;

	auto elapsed_time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start_time).count();

	for (auto hand : frame.hands()) {
		if (handOpen(hand)) {
			// Always refresh the start_time when a hand opens so that a user can
			// keep the hand open for a while before closing it to trigger the gesture
			start_time = std::chrono::steady_clock::now();

			if (!shared_memory->closing_gesture) {
				shared_memory->closing_gesture = true;
				return SKIP;
			}
		} else if (handClosed(hand) && shared_memory->closing_gesture) {
			shared_memory->closing_gesture = false;
			return BLOCK;
		} else if (elapsed_time > 400) {
			shared_memory->closing_gesture = false;
			return SKIP;
		}
	}

	return SKIP;
}

void CloseWindowMode::action(const Frame& frame) {
	// Get window information from window below pointer, extract the id, and pass that back to xdotool to kill
	system("WINDOW=$(xdotool getmouselocation --shell | sed -r -n 's/^WINDOW=//p'); WINDOWNAME=$(xdotool getwindowname $WINDOW); if [ \"$WINDOWNAME\" != \"LeapMotionXKeyboard\" ]; then xdotool windowkill $WINDOW; fi");
}
