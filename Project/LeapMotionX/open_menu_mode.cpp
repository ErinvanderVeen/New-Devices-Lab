#include "open_menu_mode.hpp"
#include <numeric>

using namespace Leap;

OpenMenuMode::OpenMenuMode(SharedMemory* shared_memory) {
	this->shared_memory = shared_memory;
}

std::pair<double, double> OpenMenuMode::calculateFingerDistance(const Hand& hand) {
	const FingerList fingers = hand.fingers();

	// Calculate the middle point of the fingers
	Vector middle_point;
	for (auto finger : fingers) {
		middle_point += finger.tipPosition();
	}
	middle_point /= fingers.count();

	// Calculate the distance of each finger to the middle point
	std::vector<float> distances;
	for (auto finger : fingers) {
		distances.push_back(middle_point.distanceTo(finger.tipPosition()));
	}

	// Calculate the mean distance from the middle point
	// This helps us know how far open the hand is
	double sum = std::accumulate(distances.begin(), distances.end(), 0.0);
	double mean = sum / distances.size();

	// Calculate the standard deviation of the finger distances. If this
	// is low we know the fingers are spread evenly around the middle point
	double sq_sum = std::inner_product(distances.begin(), distances.end(), distances.begin(), 0.0);
	double std_dev = std::sqrt(sq_sum / (distances.size() - 1) - mean * mean * distances.size() / (distances.size() - 1));

	return std::make_pair(mean, std_dev);
}

bool OpenMenuMode::handOpen(const Hand& hand) {
	std::pair<double, double> distance = calculateFingerDistance(hand);
	return distance.first > 45.0f && distance.second < 23.0f;
}

bool OpenMenuMode::handClosed(const Hand& hand) {
	std::pair<double, double> distance = calculateFingerDistance(hand);
	return distance.first < 30.0f && distance.second < 10.0f;
}

InputMode::Control OpenMenuMode::takeControl(const Frame& frame) {
	// We are already doing the closing gesture which can interfere with the
	// opening gesture so we simply don't consider the opening gesture
	if (shared_memory->closing_gesture)
		return SKIP;

	auto elapsed_time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start_time).count();

	for (auto hand : frame.hands()) {
		if (handClosed(hand)) {
			// Always refresh the start_time when a hand is closed so that a user can
			// keep the hand closed for a while before closing it to trigger the gesture
			start_time = std::chrono::steady_clock::now();

			if (!shared_memory->opening_gesture) {
				shared_memory->opening_gesture = true;
				return SKIP;
			}
		} else if (handOpen(hand) && shared_memory->opening_gesture) {
			shared_memory->opening_gesture = false;
			return BLOCK;
		} else if (elapsed_time > 400) {
			shared_memory->opening_gesture = false;
			return SKIP;
		}
	}

	return SKIP;
}

void OpenMenuMode::action(const Frame& frame) {
	system("xdotool key Super_L");
}
