#include "arbitrator.hpp"

using namespace Leap;

void Arbitrator::onInit(const Controller& controller) {
	std::cout << "Initialized Listener" << std::endl;
}

void Arbitrator::onConnect(const Controller& controller) {
	std::cout << "Connected Listener" << std::endl;
}

void Arbitrator::onDisconnect(const Controller& controller) {
	std::cout << "Disconnected Listener" << std::endl;
}

void Arbitrator::onExit(const Controller& controller) {
	std::cout << "Exited Listener" << std::endl;
}

void Arbitrator::onFrame(const Controller& controller) {
	const Frame frame = controller.frame();

	for (auto mode : modes) {
		InputMode::Control control = mode->takeControl(frame);
		switch (control) {
			case InputMode::SKIP:
				// Do nothing
				break;
			case InputMode::BLOCK:
				// Take action and stop loop
				mode->action(frame);
				return;
			case InputMode::PASS:
				// Take action and continue to the next potential mode
				mode->action(frame);
				break;
		}
	}
}

void Arbitrator::apply(InputMode& mode) {
	modes.push_back(&mode);
}
