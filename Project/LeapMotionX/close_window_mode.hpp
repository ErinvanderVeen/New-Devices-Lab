#ifndef CLOSE_WINDOW_MODE_H
#define CLOSE_WINDOW_MODE_H

#include <string>
#include <iterator>
#include <utility>
#include <chrono>
#include "Leap.h"
#include "input_mode.hpp"
#include "shared_memory.hpp"

class CloseWindowMode : public InputMode {
public:
	CloseWindowMode(SharedMemory*);
	Control takeControl(const Leap::Frame&);
	void action(const Leap::Frame&);

private:
	std::pair<double, double> calculateFingerDistance(const Leap::Hand&);
	bool handOpen(const Leap::Hand&);
	bool handClosed(const Leap::Hand&);
	SharedMemory* shared_memory;
	std::chrono::steady_clock::time_point start_time;
};

#endif
