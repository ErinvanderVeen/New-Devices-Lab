#include "cursor_mode.hpp"

using namespace Leap;

// In order for us to take control the following things must hold:
// The thumb is ignored (for usability reasons)
// The index finger must be extended
// None of the other three fingers must be extended
bool CursorMode::handTakeControl(const Hand& hand) {
	const FingerList extended_fingers = hand.fingers().extended();

	// One of the three forbidden fingers is extended
	if (!extended_fingers.fingerType(Finger::TYPE_MIDDLE).isEmpty() ||
			!extended_fingers.fingerType(Finger::TYPE_RING).isEmpty() ||
			!extended_fingers.fingerType(Finger::TYPE_PINKY).isEmpty())
		return false;

	// Index finger is extended and none of the other fingers is extended
	if (!extended_fingers.fingerType(Finger::TYPE_INDEX).isEmpty())
		return true;

	// Either the index finger was not extended or the leap motion only saw a thumb
	return false;
}

InputMode::Control CursorMode::takeControl(const Frame& frame) {
	for (auto hand : frame.hands()) {
		if (handTakeControl(hand)) {
			return BLOCK;
		}
	}

	return SKIP;
}

void CursorMode::action(const Frame& frame) {
	for (auto hand : frame.hands()) {
		// Get fingers
		const FingerList fingers = hand.fingers();

		const Pointable index_finger = fingers.fingerType(Finger::TYPE_INDEX)[0];

		const Vector stabilized_finger_vector = index_finger.stabilizedTipPosition();
		float finger_x = stabilized_finger_vector.x;
		float finger_y = stabilized_finger_vector.y;

		// Values arbitrarily chosen but takes a 16:9 aspect ratio into account
		if (finger_x >= -160 && finger_x <= 160 && finger_y >= 150 && finger_y <= 330) {
			int x = (int) ((finger_x + 160) / 320 * 1920);
			int y = (int) ((-finger_y + 330) / 180 * 1080);
			system(("xdotool mousemove " + std::to_string(x) + " " + std::to_string(y)).c_str());
			return;
		}
	}
}
