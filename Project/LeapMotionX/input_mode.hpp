#ifndef INPUT_MODE_H
#define INPUT_MODE_H

#include "Leap.h"

class InputMode {
public:
	enum Control { SKIP, BLOCK, PASS };
	virtual Control takeControl(const Leap::Frame&) = 0;
	virtual void action(const Leap::Frame&) = 0;
};

#endif
