#ifndef LEFT_CLICK_MODE_H
#define LEFT_CLICK_MODE_H

#include <string>
#include <iterator>
#include "Leap.h"
#include "input_mode.hpp"

class LeftClickMode : public InputMode {
public:
	Control takeControl(const Leap::Frame&);
	void action(const Leap::Frame&);

private:
	bool mouse_down = false;
	bool handTakeControl(const Leap::Hand&);
};

#endif
