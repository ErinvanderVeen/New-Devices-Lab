#ifndef SCROLL_MODE_H
#define SCROLL_MODE_H

#include <string>
#include <iterator>
#include "Leap.h"
#include "input_mode.hpp"

class ScrollMode : public InputMode {
public:
	Control takeControl(const Leap::Frame&);
	void action(const Leap::Frame&);

private:
	bool handTakeControl(const Leap::Hand&);
	int pitch = 0;
};

#endif
