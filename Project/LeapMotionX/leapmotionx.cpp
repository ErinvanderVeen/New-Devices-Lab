#include <iostream>
#include <cstring>
#include "Leap.h"
#include "shared_memory.hpp"
#include "arbitrator.hpp"
#include "left_click_mode.hpp"
#include "close_window_mode.hpp"
#include "cursor_mode.hpp"
#include "scroll_mode.hpp"
#include "open_menu_mode.hpp"

using namespace Leap;

int main(int argc, char** argv) {
	Arbitrator arbitrator;
	Controller controller;

	controller.addListener(arbitrator);

	// Set policy to receive frames when application is in background
	controller.setPolicy(Leap::Controller::POLICY_BACKGROUND_FRAMES);

	SharedMemory shared_memory;

	// Add all modes to the arbitrator, note that the order is relevant
	CloseWindowMode close_window_mode(&shared_memory);
	arbitrator.apply(close_window_mode);

	OpenMenuMode open_menu_mode(&shared_memory);
	arbitrator.apply(open_menu_mode);

	LeftClickMode left_click_mode;
	arbitrator.apply(left_click_mode);

	CursorMode cursor_mode;
	arbitrator.apply(cursor_mode);

	ScrollMode scroll_mode;
	arbitrator.apply(scroll_mode);

	// Keep this process running until Enter is pressed
	std::cout << "Press Enter to quit..." << std::endl;
	std::cin.get();

	// Remove the sample listener when done
	controller.removeListener(arbitrator);

	return 0;
}
