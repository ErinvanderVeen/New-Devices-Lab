#include "scroll_mode.hpp"

using namespace Leap;

// For this mode the thumb is excluded
// Check if every finger is extended and the Intermediate bone is
// close enough to the next one. If that is the case and the hand
// has a pitch above our threshold we want to scroll
InputMode::Control ScrollMode::takeControl(const Frame& frame) {
	for (auto hand : frame.hands()) {
		FingerList fl = hand.fingers();

		// count() - 1 since the previous finger will already next
		// if the last finger is within range
		for (int i = 0; i < fl.count() - 1; i++) {
			// We don't care where the thumb is. The thumb doesn't necessarily
			// have to be present so we can't change i to start from 1.
			if (fl[i].type() == Finger::TYPE_THUMB)
				continue;

			// We only care if the fingers are extended
			if (!fl[i].isExtended())
				return SKIP;

			// If any finger fails the closeness check we will not perform this mode
			if (fl[i].bone(Bone::TYPE_INTERMEDIATE).center().distanceTo(fl[i+1].bone(Bone::TYPE_INTERMEDIATE).center()) > 30.0f)
				return SKIP;
		}

		// We could've only reached here if the correct hand gesture is performed.

		// If the hand pitch is high/low enough enough we want to scroll up/down
		pitch = hand.direction().pitch() * RAD_TO_DEG;
		if (pitch > 30 || pitch < -30) {
			return BLOCK;
		}
	}

	return SKIP;
}

void ScrollMode::action(const Frame& frame) {
	if (pitch > 0) {
		system("xdotool click 4");
	} else if (pitch < 0) {
		system("xdotool click 5");
	}
}
