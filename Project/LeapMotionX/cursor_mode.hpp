#ifndef CURSOR_MODE_H
#define CURSOR_MODE_H

#include <string>
#include <iterator>
#include "Leap.h"
#include "input_mode.hpp"

class CursorMode : public InputMode {
public:
	Control takeControl(const Leap::Frame&);
	void action(const Leap::Frame&);

private:
	bool handTakeControl(const Leap::Hand&);
};

#endif
