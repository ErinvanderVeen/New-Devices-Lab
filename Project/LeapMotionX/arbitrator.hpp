#ifndef ARBITRATOR_H
#define ARBITRATOR_H

#include <string>
#include <iterator>
#include <list>
#include "Leap.h"
#include "input_mode.hpp"

class Arbitrator : public Leap::Listener {
public:
	void onInit(const Leap::Controller&);
	void onConnect(const Leap::Controller&);
	void onDisconnect(const Leap::Controller&);
	void onExit(const Leap::Controller&);
	void onFrame(const Leap::Controller&);
	void apply(InputMode&);

private:
	std::list<InputMode*> modes;
};

#endif
