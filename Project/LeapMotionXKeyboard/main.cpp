// Example implementation from:
// https://stackoverflow.com/questions/16832581/how-do-i-make-a-gtkwindow-background-transparent-on-linux
// Note that it is _heavily_ changed as transparency is no longer used extensively
#include "keyboard.hpp"
#include <gtkmm/application.h>

int main (int argc, char *argv[])
{
    Glib::RefPtr<Gtk::Application> app = Gtk::Application::create("leapmotionx.input.handler");

    Keyboard keyboard;

    //Shows the window and returns when it is closed.
    return app->run(keyboard);
}
