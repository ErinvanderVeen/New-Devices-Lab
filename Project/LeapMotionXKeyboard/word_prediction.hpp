#ifndef WORD_PREDICTION_H
#define WORD_PREDICTION_H

#include <iostream>
#include "symspell.hpp"

class WordPrediction {
public:
	WordPrediction(std::string);
	virtual ~WordPrediction();
	void pushCharacter(char);
	void popCharacter();
	std::vector<std::string> getPredictions();
	std::string input;

private:
	SymSpell sym_spell;
};

#endif /* WORD_PREDICTION_H */
