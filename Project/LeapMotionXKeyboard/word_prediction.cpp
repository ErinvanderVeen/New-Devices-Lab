#include "word_prediction.hpp"

WordPrediction::WordPrediction(std::string filename) {
	// Load the dictionary but save a hashed variant
	// if we have processed it before to speed up loading
	if(!std::ifstream("hashed_" + filename)) {
		sym_spell.CreateDictionary(filename);
		sym_spell.Save("hashed_" + filename);
	} else {
		sym_spell.Load("hashed_" + filename);
	}
	sym_spell.verbose = 2;
}

WordPrediction::~WordPrediction() {
}

void WordPrediction::pushCharacter(char character) {
	input.push_back(character);
}

void WordPrediction::popCharacter() {
	if (!input.empty()) {
		input.pop_back();
	}
}

std::vector<std::string> WordPrediction::getPredictions() {
	// SymSpell only deals well with the input if it has the same casing as the dictionary
	std::string input_lower = input;
	std::transform(input.begin(), input.end(), input_lower.begin(), ::tolower);

	std::vector<suggestItem> results = sym_spell.Correct(input_lower);
	// Reverse since SymSpellPlusPlus returns furthest words first which is undesirable
	std::reverse(results.begin(), results.end());

	std::vector<std::string> predictions;
	for (suggestItem correction : results) {
		predictions.push_back(correction.term);
	}

	return predictions;
}
