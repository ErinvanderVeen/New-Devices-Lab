#ifndef KEYBOARD_H_
#define KEYBOARD_H_

#include <iostream>
#include <vector>
#include <chrono>
#include <gtk/gtk.h>
#include <gtkmm/window.h>
#include <gtkmm/button.h>
#include <gtkmm/grid.h>
#include <gtkmm/box.h>
#include <gtkmm/hvbox.h>
#include <gtkmm/label.h>
#include <gtkmm/eventbox.h>
#include "word_prediction.hpp"

class Keyboard;
class KeyboardButton;
class KeyboardEventBox;

class Keyboard : public Gtk::Window {
public:
	Keyboard();
	~Keyboard();
	void keyHandling(KeyboardButton*);
	void wordEnteredCallback();

private:
	// Overrided from Gtk::Window
	bool on_draw(const ::Cairo::RefPtr< ::Cairo::Context>&);
	void on_screen_changed(const Glib::RefPtr<Gdk::Screen>&);
	void updateLabels();
	Gtk::Grid* createGrid();

	const int window_width = 1920;
	const int window_height = 180;

	bool supports_alpha = false;
	WordPrediction word_predict;

	std::vector<std::string> button_labels = {"SHIFT", "q", "a", "z", "w", "s", "x", "e", "d", "c", "r",
	"f", "v", "t", "SPACE", "g", "b", "y", "h", "n", "u", "j", "m", "i", "k", "o", "l", "p", ".", "ENTER", "←"};
	std::vector<KeyboardButton*> buttons;
	std::vector<KeyboardEventBox*> labels;
	std::vector<std::string> predictions;
};

class KeyboardButton : public Gtk::Button {
public:
	KeyboardButton(Keyboard*, std::string);
	~KeyboardButton();
	void switchLetterCasing();

private:
	bool on_enter_notify_event(GdkEventCrossing*);
	bool on_leave_notify_event(GdkEventCrossing*);

	Keyboard* keyboard;
	std::chrono::steady_clock::time_point enter_time;
};

class KeyboardEventBox : public Gtk::EventBox {
public:
	KeyboardEventBox(Keyboard*);
	~KeyboardEventBox();
	void set_label(std::string);

private:
	bool on_enter_notify_event(GdkEventCrossing*);
	bool on_leave_notify_event(GdkEventCrossing*);

	Keyboard* keyboard;
	std::chrono::steady_clock::time_point enter_time;
};

#endif /* KEYBOARD_H_ */
