#include "keyboard.hpp"

Keyboard::Keyboard() : word_predict("google-10000-english.txt") {
	// Set up the top-level window.
	set_title("LeapMotionXKeyboard");
	set_default_size(window_width, window_height);
	set_decorated(false);
	add_events(Gdk::ENTER_NOTIFY_MASK);
	set_position(Gtk::WIN_POS_CENTER);
	set_app_paintable(true);
	set_keep_above(true);

	// Signal handlers
	signal_draw().connect(sigc::mem_fun(*this, &Keyboard::on_draw));
	signal_screen_changed().connect(sigc::mem_fun(*this, &Keyboard::on_screen_changed));

	add(*createGrid());

	// Widgets
	on_screen_changed(get_screen());

	// Show the window and all its children.
	show_all();
}

Keyboard::~Keyboard() {
}

Gtk::Grid* Keyboard::createGrid() {
	Gtk::Grid* grid = new Gtk::Grid();
	grid->insert_row(0);

	// Add prediction box
	Gtk::HBox* h_box = new Gtk::HBox();
	h_box->set_homogeneous(true);
	h_box->set_size_request(window_width, -1);
	// For now we want to show our typed word and 5 suggestions
	for (int i = 0; i < 1 + 5; i++) {
		labels.push_back(new KeyboardEventBox(this));
		labels.back()->set_size_request(-1, (int) (window_height * 0.25));
		h_box->pack_start(*labels.back());
	}
	grid->attach(*h_box, 0, 0, 1, 1);

	// Add buttons
	Gtk::Box* box = new Gtk::Box();
	box->set_size_request(window_width, -1);
	for (auto label : button_labels) {
		buttons.push_back(new KeyboardButton(this, label));
		buttons.back()->set_size_request(-1, (int) (window_height * 0.75));
		box->pack_start(*buttons.back());
	}
	grid->attach(*box, 0, 1, 1, 1);

	return grid;
}


// Since we cannot use a switch statement on a string we use a constexpr that converts
// strings to an integer value. Source: https://stackoverflow.com/questions/2111667/compile-time-string-hashing
constexpr unsigned int str2int(const char* str, int h = 0) {
	return !str[h] ? 5381 : (str2int(str, h+1) * 33) ^ str[h];
}

void Keyboard::keyHandling(KeyboardButton* keyboard_button) {
	switch (str2int(keyboard_button->get_label().c_str())) {
		case str2int("SPACE"):
			system("xdotool key space");
			break;
		case str2int("SHIFT"):
			for (auto button : buttons) {
				button->switchLetterCasing();
			}
			break;
		case str2int("."):
			system("xdotool key period");
			break;
		case str2int("ENTER"):
			system("xdotool key Return");
			break;
		case str2int("←"):
			// Depending on if we've already entered something or not
			// we want backspace to behave differently
			if (word_predict.input.size() > 0) {
				word_predict.popCharacter();
				predictions = word_predict.getPredictions();
				updateLabels();
			} else {
				system("xdotool key BackSpace");
			}
			break;
		default:
			word_predict.pushCharacter(keyboard_button->get_label()[0]);
			predictions = word_predict.getPredictions();
			updateLabels();
			break;
	}
}

// This method gets called when a word has been entered and we want to clear all current
// predictions and input
void Keyboard::wordEnteredCallback() {
	word_predict.input = "";
	predictions.clear();
	updateLabels();
}

void Keyboard::updateLabels() {
	labels[0]->set_label(word_predict.input);
	for (uint i = 1; i < labels.size(); i++) {
		labels[i]->set_label(i <= predictions.size() ? predictions[i-1] : "");
	}
}

bool Keyboard::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
	cr->save();
	if (supports_alpha) {
		// transparent
		cr->set_source_rgba(0.0, 0.0, 0.0, 0.65);
	} else {
		// opaque
		cr->set_source_rgb(0.0, 0.0, 0.0);
	}
	cr->set_operator(Cairo::OPERATOR_SOURCE);
	cr->paint();
	cr->restore();

	// Move the window to the bottom. This can not be done at the moment of creation
	// because most window managers ignore move when starting. This assumes a 1920x1080 resolution
	this->move(0, 1080 - window_height);

	return Gtk::Window::on_draw(cr);
}

// Checks to see if the display supports alpha channels
void Keyboard::on_screen_changed(const Glib::RefPtr<Gdk::Screen>& previous_screen) {
	auto screen = get_screen();
	auto visual = screen->get_rgba_visual();

	if (visual) {
		supports_alpha = TRUE;
	} else {
		std::cout << "No alpha channel support" << std::endl;
	}

	gtk_widget_set_visual(GTK_WIDGET(gobj()), visual->gobj());
}

KeyboardButton::KeyboardButton(Keyboard* keyboard, std::string label) : Gtk::Button(label) {
	this->keyboard = keyboard;
}

KeyboardButton::~KeyboardButton() {
}

bool KeyboardButton::on_enter_notify_event(GdkEventCrossing*) {
	enter_time = std::chrono::steady_clock::now();
	return true;
}

bool KeyboardButton::on_leave_notify_event(GdkEventCrossing*) {
	auto elapsed_time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - enter_time).count();

	// Only register a "click" if we are above a certain elapsed time threshold
	if (elapsed_time > 400) {
		keyboard->keyHandling(this);
	}

	return true;
}

void KeyboardButton::switchLetterCasing() {
	std::string label = this->get_label();

	if (label.size() == 1) {
		if (std::isupper(label[0])) {
			label[0] = std::tolower(label[0]);
		} else if (std::islower(label[0])) {
			label[0] = std::toupper(label[0]);
		}
		this->set_label(label);
	}
}

KeyboardEventBox::KeyboardEventBox(Keyboard* keyboard) {
	this->keyboard = keyboard;
	this->add_label("");
}

KeyboardEventBox::~KeyboardEventBox() {
}

void KeyboardEventBox::set_label(std::string label) {
	((Gtk::Label*) this->get_child())->set_label(label);
}

bool KeyboardEventBox::on_enter_notify_event(GdkEventCrossing*) {
	enter_time = std::chrono::steady_clock::now();
	return true;
}

bool KeyboardEventBox::on_leave_notify_event(GdkEventCrossing*) {
	auto elapsed_time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - enter_time).count();

	// Only register a "click" if we are above a certain elapsed time threshold and the word we want to enter is not empty
	if (elapsed_time > 400 && !((Gtk::Label*) this->get_child())->get_label().empty()) {
		system((std::string("xdotool type ") + ((Gtk::Label*) this->get_child())->get_label()).c_str());
		keyboard->wordEnteredCallback();
	}

	return true;
}
